import hashlib
import time
import csv 
t0 = time.time()


#ХЭШ строки из файла
def hash_bar(filename):
    h = hashlib.sha1()
    with open(filename, "rb") as f:
        a = f.readline()
        h.update(a)
    
    return h.hexdigest()

# ХЭШ файла
def hash_file(filename):
    h = hashlib.sha1()
    with open (filename, "rb") as f:
        text = f.read()
        h.update(text)
        
    return h.hexdigest()

# ХЭШ ФИО в 2м коде

def hash_bi_FIO(filename):
    h = hashlib.sha1()
    with open(filename, "r") as f:
        a = f.readline()
        z = "".join(format(ord(x),"b") for x in a)
        k = z.encode()
        h.update(k)
    return h.hexdigest()
# Меняем бит в ФИО
def change_bit_FIO(filename):
    h = hashlib.sha1()
    with open(filename,"r") as f:
        a = f.readline()
        z = "".join(format(ord(x),"b") for x in a)
        z = z[:2] + "1" + z[3:]
        k = z.encode()
        h.update(k)
    return h.hexdigest()
# Меняем бит файла
def change_bit_file(filename):
    h= hashlib.sha1()
    with open (filename,"r+") as f:
        f.seek(4)
        f.write(chr(0x00))
        f.close()
    with open (filename, "rb") as f:
        text = f.read()
        
        h.update(text)
    return h.hexdigest()
res = open("Vladislav.csv","a+")
message = hash_file("Vlad.txt")

message1 = hash_bar("Vlad.txt")
message3 = hash_bi_FIO("Vlad.txt")
message4 = change_bit_FIO("Vlad.txt")
message5 = change_bit_file("Vlad.txt")                    
print("Хэш строки: ", message1)
res.write(message1 + "\n")
res.write(message + "\n")
res.write(message3 + "\n")
res.write(message4 + "\n")
res.write(message5 + "\n")
res.close()
print("Хэш файла: ", message)
print("Хэш ФИО в 2-м коде: ", message3)
print("Изменили бит в ФИО: ", message4)
print( "Меняем бит файла: ", message5)
t1 = time.time() - t0
print("Время выполнения программы: ", t1)


